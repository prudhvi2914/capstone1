//
//  ViewController.swift
//  KillAliens
//
//  Created by satram prudhvi on 2019-12-04.
//  Copyright © 2019 satram prudhvi. All rights reserved.
//

import UIKit
import ARKit
import  SpriteKit

class ViewController: UIViewController {

    @IBOutlet var sceneView: ARSKView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let scene = SKScene(fileNamed: "Scene"){
            sceneView.presentScene(scene)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           
           let configuration = ARWorldTrackingConfiguration()
           
           sceneView.session.run(configuration)
       }
       
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           sceneView.session.pause()
       }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension ViewController : ARSKViewDelegate {
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}



